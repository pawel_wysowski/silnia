﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;

namespace Silnia
{
    public partial class Form1 : Form
    {

        public delegate void ClickAction();
        public static event ClickAction OnClicked;

        Stopwatch stopwatch1 = new Stopwatch();
        Stopwatch stopwatch2 = new Stopwatch();

        
        public bool button2clicked;

        public Form1()
        {
            InitializeComponent();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            button2clicked = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            stopwatch2.Reset();
            stopwatch1.Reset();
            Thread silniaR = new Thread(() =>
            {
                decimal r = silniaRekurencyjnie(numericUpDown1.Value);
                MessageBox.Show("WYNIK REKURENCJA: " + r);
            });

            Thread silniaI = new Thread(() =>
            {
                decimal s = silniaIteracyjnie(numericUpDown1.Value);
                MessageBox.Show("WYNIK ITERACJA: " + s );
            });


            stopwatch1.Start();
            silniaR.Start();

            if(button2clicked==true)
            {
                silniaR.Abort();
                silniaI.Abort();
                button2clicked = false;
            }

            stopwatch1.Stop();
           






            stopwatch2.Start();
            silniaI.Start();
            stopwatch2.Stop();
           

            TimeSpan t1 = stopwatch1.Elapsed;
            TimeSpan t2 = stopwatch2.Elapsed;
            label1.Text = t1.ToString();
            label2.Text = t2.ToString();

        }

        public decimal silniaRekurencyjnie(decimal i)
        { 
            if (i < 1)
                return 1;
            else
            return i*silniaRekurencyjnie(i-1);

        }


        private decimal silniaIteracyjnie(decimal i)
        {
            decimal silnia = 1;
            for(decimal j = 1; j<=i; j++)
            {
                silnia *= j;
            }
            return silnia;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
